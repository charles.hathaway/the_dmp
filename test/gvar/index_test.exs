defmodule DistributedMumpsProgram.GVar.IndexTest do
  use ExUnit.Case
  doctest DistributedMumpsProgram.GVar.Index

  alias DistributedMumpsProgram.GVar.{Index,MemoryServer}

  test "can set index" do
     v = %Index{}
     v = Index.set_value(["abc"], "Hot dog", v)
     assert v == %DistributedMumpsProgram.GVar.Index{filepointer: nil,
      free_blocks: [], end_block: 37, keys: [tree: {1, {"abc", [tree: {0, nil},
      block: %DistributedMumpsProgram.GVar.Block{location: 0, size: 37}], nil,
      nil}}, block: nil]}
  end

  test "can set index 2 levels in" do
     v = %Index{}
     v = Index.set_value(["abc", "dog", "cat"], 123, v)
     assert v == %DistributedMumpsProgram.GVar.Index{filepointer: nil,
        free_blocks: [], end_block: 49, keys: [tree:
          {1, {"abc", [tree: {1, {"dog", [tree: {1, {"cat", [tree: {0, nil},
          block: %DistributedMumpsProgram.GVar.Block{location: 0, size: 49}],
          nil, nil}}, block: nil], nil, nil}}, block: nil], nil, nil}},
          block: nil]}
  end

  test "can get non-existent block from index" do
     v = %DistributedMumpsProgram.GVar.Index{end_block: 0, free_blocks: [],
             keys: [tree: {0, nil}, block: nil]}
     r = Index.get_value(["abc"], v)
     assert r == nil
  end

  test "can set and get value from index with memory server" do
     v = %DistributedMumpsProgram.GVar.Index{end_block: 0, free_blocks: [],
             keys: [tree: {0, nil}, block: nil], filepointer: MemoryServer.make_instance}
     v = Index.set_value(["abc"], 123, v)
     r = Index.get_value(["abc"], v)
     assert r == "123"
  end

  test "can set and get value with multiple subscripts from index with memory server" do
     v = %DistributedMumpsProgram.GVar.Index{filepointer: MemoryServer.make_instance}
     v = Index.set_value(["cats", "jojo"], 123, v)
     r = Index.get_value(["cats", "jojo"], v)
     assert r == "123"
     v = Index.set_value(["cats", "max", "age"], 123, v)
     r = Index.get_value(["cats", "max", "age"], v)
     assert r == "123"
  end

  test "can save index" do
    {:ok, fd} = :file.open('', [:write, :read, :binary, :ram])
    v = %Index{end_block: 5}
    Index.write_index(fd, v)
    :file.position(fd, 0)
    index2 = Index.read_index(fd)
    assert v == index2
  end

  test "kill value" do
     v = %DistributedMumpsProgram.GVar.Index{end_block: 0, free_blocks: [],
             keys: [tree: {0, nil}, block: nil], filepointer: MemoryServer.make_instance}
     v = Index.set_value(["abc"], 123, v)
     v = Index.kill_value(["abc"], v)
     assert length(v.free_blocks) == 1
  end
end
