defmodule DistributedMumpsProgram.BlackBoxTest do
  use ExUnit.Case
  doctest DistributedMumpsProgram

  test "the truth" do
    assert 1 + 1 == 2
  end

  test "simple addition works" do
     state = DistributedMumpsProgram.run_line('SET A=5+6*2')
     assert state.symbols['A'] == [tree: {0, nil}, value: ['22.0', ""]]
  end
end
