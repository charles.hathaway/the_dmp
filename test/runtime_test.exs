defmodule DistributedMumpsProgramTest.Runtime do
  use ExUnit.Case
  alias DistributedMumpsProgram.{Runtime}
  doctest DistributedMumpsProgram.Runtime

  test "basic new" do
    result = Runtime.new([{:symbol, 1, 'A'}], %{symbols: %{}})
    assert result.symbols == %{}
  end

  test "basic set" do
    result = Runtime.set([{{:local, {:symbol, 1, 'A'}}, {:expr, 1, 'hello world!'}}], %{symbols: %{}})
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['hello world!', ""]]}
  end

  test "basic kill" do
    result = Runtime.kill([{:symbol, 1, 'A'}], %{symbols: %{'A' => ['hello world!']}})
    assert result.symbols == %{}
  end

  test "multiple sets" do
    result = Runtime.set([{{:local, {:symbol, 1, 'A'}}, {:expr, 1, 'hello world!'}}], %{symbols: %{}})
    assert result.symbols ==  %{'A' => [tree: {0, nil}, value: ['hello world!', ""]]}
    result = Runtime.set([{{:local, {:symbol, 1, 'A'}}, {:expr, 1, 'Jojo is a kitty cat'}}], result)
    assert result.symbols == %{'A' => [tree: {0, nil},
               value: ['Jojo is a kitty cat', 'hello world!', ""]]}
    result = Runtime.set([{{:local, {:symbol, 1, 'B'}}, {:expr, 1, 'Max is a kitten'}}], result)
    assert result.symbols == %{'A' => [tree: {0, nil},
               value: ['Jojo is a kitty cat', 'hello world!', ""]],
              'B' => [tree: {0, nil}, value: ['Max is a kitten', ""]]}
  end

  test "basic new set kill" do
    result = Runtime.new([{:symbol, 1, 'A'}], %{symbols: %{}})
    assert result.symbols == %{}
    result = Runtime.set([{{:local, {:symbol, 1, 'A'}}, {:expr, 1, 'hello world!'}}], result)
    assert result.symbols == %{'A' => ['hello world!']}
    result = Runtime.kill([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{}
  end

  test "new set new kill" do
    result = Runtime.new([{:symbol, 1, 'A'}], %{symbols: %{}})
    assert result.symbols == %{}
    result = Runtime.set([{{:local, {:symbol, 1, 'A'}}, {:expr, 1, 'hello world!'}}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['hello world!', ""]]}
    result = Runtime.new([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['', 'hello world!', ""]]}
    result = Runtime.kill([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['hello world!', ""]]}
  end

  test "new set new kill kill" do
    result = Runtime.new([{:symbol, 1, 'A'}], %{symbols: %{}})
    assert result.symbols == %{}
    result = Runtime.set([{{:local, {:symbol, 1, 'A'}}, {:expr, 1, 'hello world!'}}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['hello world!', ""]]}
    result = Runtime.new([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['', 'hello world!', ""]]}
    result = Runtime.kill([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['hello world!', ""]]}
    result = Runtime.kill([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: [""]]}
  end

  test "new set new kill kill kill" do
    result = Runtime.new([{:symbol, 1, 'A'}], %{symbols: %{}})
    assert result.symbols == %{}
    result = Runtime.set([{{:local, {:symbol, 1, 'A'}}, {:expr, 1, 'hello world!'}}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['hello world!', ""]]}
    result = Runtime.new([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['', 'hello world!', ""]]}
    result = Runtime.kill([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: ['hello world!', ""]]}
    result = Runtime.kill([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: [""]]}
    result = Runtime.kill([{:symbol, 1, 'A'}], result)
    assert result.symbols == %{'A' => [tree: {0, nil}, value: []]}
  end

  test "set of undefined variable" do
    result = Runtime.set([{{:local, {:symbol, 1, 'B'}}, {:local, {:symbol, 1, 'Z'}}}], %{symbols: %{}})
    assert result.symbols ==%{'B' => [tree: {0, nil}, value: ["", ""]]}
  end
end
