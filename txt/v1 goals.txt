Goal:

Read instruction as intepreter, execute, supporting:
 SET
 KILL
 USE

Where we supoport:
 atom :: [A-Za-z][A-Za-z0-9]*
 expr :: "[^"]*"

Grammar:
  LINE :: STATEMENT LINE
  STATEMENT_LINE :: STATEMENT STATEMENT_LINE | _
  STATEMENT :: SET_EXPR | KILL_EXPR | NEW_EXPR
  SET_EXPR :: <SET> SET_TAIL
  SET_TAIL :: atom = expr SET_TAIL_TAIL
  SET_TAIL_TAIL :: ,atom=exprSET_TAIL | _
  KILL_EXPR :: <KILL> KILL_TAIL
  KILL_TAIL :: atom KILL_TAIL_TAIL
  KILL_TAIL_TAIL :: ,KILL_TAIL | _
  NEW_EXPR :: <NEW> NEW_TAIL
  NEW_TAIL :: atom NEW_TAIL_TAIL
  NEW_TAIL_TAIL :: ,NEW_TAIL | _

FIRST(LINE) = [<SET>, <KILL>, <NEW>]
FIRST(SET_EXPR) = [<SET>]
FIRST(KILL_EXPR) = [<KILL>]
FIRST(NEW_EXPR) = [<NEW>]

NEW A,B SET A="5",B="4" KILL A,B

LINE
> STATEMENT_LINE
> STATEMENT STATEMENT_LINE
> STATEMENT STATEMENT_LINE

Save to flat file, in the form

[{set,1}, {atom,1,"A"}, {'=',1}, {expr,1,"\"Hello \"\"world\"\"!\""}, {ws, 1}]
