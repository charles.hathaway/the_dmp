# DistributedMumpsProgram

A toy MUMPS implementation written Elixir

## Installation

Install Elixir as documented at [https://elixir-lang.org/](https://elixir-lang.org/).

Once installed, you can build the app simply by running:
```
mix deps.get
```

From the root directory of the this repository.

## Running

You can launch a shell by running:

```
# <If windows>
iex --werl --sname foo -S mix
# <Else>
iex --sname foo -S mix
DistributedMumpsProgram.run
```

And launch a second shell if you wish to test features such as LOCK or SET ^GLOBAL.

```
# <If windows>
iex --werl --sname bar --remsh foo@<your hostname> -S mix
# <Else>
iex --sname bar --remsh foo@<your hostname> -S mix
DistributedMumpsProgram.run
```

Before exiting, you need to write the index file from the Elixir shell.
At some point in the future, an automatic write or the ability to restore the
 index file from the database will be added.

```
DMP> Q
:ok
iex(foo@blaze)34> DistributedMumpsProgram.GVarServer.write_indexs
:ok
iex(foo@blaze)35>
```

## Features

Commands: IF, ELSE, FOR, KILL, SET, NEW, WRITE, LOCK, HANG, QUIT

Functions: $RANDOM

Basic database support; not production ready.

### Caveats

Due to laziness, several of these commands have one-letter names which conflict
  with the definition of a symbol. In the future, this could be remedied by
  adding special handling for each symbol. For the time being, please be aware
  of this problem.
