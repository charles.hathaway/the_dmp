Definitions.

KILL = ((KILL)|(kill)|K|k)
SET = ((SET)|(set)|s|S)
NEW = (N|n|(NEW)|(new))
WRITE = (W|w|(WRITE)|(write))
IF = ((IF)|(if)|I|i)
ELSE = ((ELSE)|(else)|E|e)
FOR = ((FOR)|(for)|F|f)
LOCK = ((LOCK)|(lock)|L|l)
HANG = ((HANG)|(hang)|H|h)
QUIT = ((QUIT)|(quit)|Q|q)
COMMA = ,
QUOTE = "
EXCLAIM = !
COLON = :
PLUS = \+
MINUS = -
EQU = =
D = [0-9]
SYMBOL = [A-Za-z][A-Za-z0-9]*
WS = [\t\s\n]
EXPR = "([^"]|(""))*"
FUNC_FORMAT = \${SYMBOL}

Rules.

{KILL}\s : {token, {kill, TokenLine}}.
{SET}\s : {token, {set, TokenLine}}.
{NEW}\s : {token, {new, TokenLine}}.
{WRITE}\s : {token, {write, TokenLine}}.
{IF}\s : {token, {if_token, TokenLine}}.
{ELSE}\s : {token, {else_token, TokenLine}}.
{FOR}\s : {token, {for_token, TokenLine}}.
{LOCK}\s : {token, {lock_token, TokenLine}}.
{HANG}\s : {token, {hang_token, TokenLine}}.
{QUIT} : {token, {quit_token, TokenLine}}.
{EQU} : {token, {'=', TokenLine}}.
{COLON} : {token, {':', TokenLine}}.
{COMMA} : {token, {',', TokenLine}}.
{PLUS} : {token, {'+', TokenLine}}.
{MINUS} : {token, {'-', TokenLine}}.
{WS} : skip_token.
! : {token, {'!', TokenLine}}.

{FUNC_FORMAT} : {token, {func, TokenLine, TokenChars}}.

{EXPR} :
  [_ | TChars] = TokenChars,
  {TokenChars2, _} = lists:split(length(TChars)-1, TChars),
  {token, {expr, TokenLine, TokenChars2}}.

{D}+ :
  {token,{expr,TokenLine,TokenChars}}.

{D}+\.{D}+((E|e)(\+|\-)?{D}+)? :
  {token,{expr,TokenLine,TokenChars}}.

{SYMBOL} :{token, {symbol, TokenLine, TokenChars}}.

\^ : {token, {hat, TokenLine}}.

('<)|('>)|(<=)|(>=)|('=) : {token, {op, TokenLine, TokenChars}}.
[\*/<>&!] : {token, {op, TokenLine, TokenChars}}.
\( : {token, {'(', TokenLine}}.
\) : {token, {')', TokenLine}}.

;.* : skip_token.

. : {token, {TokenChars, TokenLine, TokenChars}}.

Erlang code.
