Nonterminals LINE STATEMENT STATEMENTS SET_EXPR SET_TAIL PARENS WRITE_EXPR WRITE_TAIL
  KILL_EXPR KILL_TAIL NEW_EXPR NEW_TAIL SYMBOL EXPRESSION EXPRESSION_TAIL VALUE
  IF_EXPR IF_TAIL OPERATOR FOR_EXPR LOCK_EXPR LOCK_VALUE LOCK_TAIL HANG_EXPR
  EXPRESSION_LIST EXPRESSION_LIST_TAIL FUNC_EXPRESSION_LIST SYMBOL_SUBSCRIPT QUIT_EXPR.
Terminals set kill new symbol expr '=' ',' end hat '(' ')' op write '!'
  if_token else_token for_token ':' '+' '-' lock_token hang_token func quit_token.
Rootsymbol LINE.

LINE -> STATEMENTS : '$1'.
STATEMENTS -> STATEMENT : '$1'.
STATEMENTS -> STATEMENTS STATEMENT : '$1' ++ '$2'.
STATEMENT -> SET_EXPR : ['$1'].
STATEMENT -> KILL_EXPR : ['$1'].
STATEMENT -> NEW_EXPR : ['$1'].
STATEMENT -> WRITE_EXPR : ['$1'].
STATEMENT -> IF_EXPR : ['$1'].
STATEMENT -> FOR_EXPR : ['$1'].
STATEMENT -> LOCK_EXPR : ['$1'].
STATEMENT -> HANG_EXPR : ['$1'].
STATEMENT -> QUIT_EXPR : ['$1'].
STATEMENT -> end : [].

SET_EXPR -> set SYMBOL '=' EXPRESSION : {set, [{'$2', '$4'}]}.
SET_EXPR -> set SYMBOL '=' EXPRESSION SET_TAIL : {set, [{'$2', '$4'}] ++ '$5'}.
SET_TAIL -> ',' SYMBOL '=' EXPRESSION : [{'$2', '$4'}].
SET_TAIL -> ',' SYMBOL '=' EXPRESSION SET_TAIL : [{'$2', '$4'}] ++ '$5'.

KILL_EXPR -> kill SYMBOL : {kill, ['$2']}.
KILL_EXPR -> kill SYMBOL KILL_TAIL : {kill, ['$2'] ++ '$3'}.
KILL_TAIL -> ',' SYMBOL : ['$2'].
KILL_TAIL -> ',' SYMBOL KILL_TAIL : ['$2'] ++ '$3'.

NEW_EXPR -> new SYMBOL : {new, ['$2']}.
NEW_EXPR -> new SYMBOL NEW_TAIL : {new, ['$2'] ++ '$3'}.
NEW_TAIL -> ',' SYMBOL : ['$2'].
NEW_TAIL -> ',' SYMBOL NEW_TAIL : ['$2'] ++ '$3'.

WRITE_EXPR -> write EXPRESSION : {write, ['$2']}.
WRITE_EXPR -> write '!' : {write, ["\n"]}.
WRITE_EXPR -> write EXPRESSION WRITE_TAIL : {write, ['$2'] ++ '$3'}.
WRITE_EXPR -> write '!' WRITE_TAIL : {write, ["\n"] ++ '$3'}.
WRITE_TAIL -> ',' EXPRESSION : ['$2'].
WRITE_TAIL -> ',' EXPRESSION WRITE_TAIL : ['$2'] ++ '$3'.
WRITE_TAIL -> ',' '!' : ["\n"].
WRITE_TAIL -> ',' '!' WRITE_TAIL : ["\n"] ++ '$3'.

% Currently, this creates a shift/reduce error
IF_EXPR -> if_token EXPRESSION STATEMENT IF_TAIL : {if_token, {'$2', '$3', '$4'}}.
IF_EXPR -> if_token EXPRESSION STATEMENT : {if_token, {'$2', '$3'}}.
IF_TAIL -> else_token STATEMENT : {else_token, '$2'}.

FOR_EXPR -> for_token SYMBOL '=' EXPRESSION ':' EXPRESSION ':' EXPRESSION STATEMENTS :
  {for_token, {'$2', '$4', '$6', '$8', '$9'}}.

LOCK_EXPR -> lock_token LOCK_VALUE LOCK_TAIL : {lock, [{'$2'}] ++ '$3'}.
LOCK_EXPR -> lock_token LOCK_VALUE : {lock, [{'$2'}]}.
LOCK_VALUE -> '+' SYMBOL : {inc, '$2'}.
LOCK_VALUE -> '-' SYMBOL : {dec, '$2'}.
LOCK_TAIL -> ',' LOCK_VALUE : [{'$2'}].
LOCK_TAIL -> ',' LOCK_VALUE LOCK_TAIL : [{'$2'}] ++ '$3'.

HANG_EXPR -> hang_token EXPRESSION : {hang, '$2'}.

QUIT_EXPR -> quit_token : quit.

SYMBOL -> symbol SYMBOL_SUBSCRIPT : {local, '$1', '$2'}.
SYMBOL -> hat symbol SYMBOL_SUBSCRIPT : {gvar, '$2', '$3'}.
SYMBOL -> symbol : {local, '$1'}.
SYMBOL -> hat symbol : {gvar, '$2'}.

SYMBOL_SUBSCRIPT -> '(' EXPRESSION_LIST ')' : '$2'.

FUNC_EXPRESSION_LIST -> '(' EXPRESSION_LIST ')' : '$2'.
EXPRESSION_LIST -> EXPRESSION : ['$1'].
EXPRESSION_LIST -> EXPRESSION EXPRESSION_LIST_TAIL : ['$1'] ++ '$2'.
EXPRESSION_LIST_TAIL -> ',' EXPRESSION_LIST : '$2'.
EXPRESSION_LIST_TAIL -> ',' EXPRESSION_LIST EXPRESSION_LIST_TAIL : ['$2'] ++ '$3'.

PARENS -> '(' EXPRESSION ')' : '$2'.
EXPRESSION -> VALUE OPERATOR VALUE EXPRESSION_TAIL : {'$2', '$1', '$3', '$4'}.
EXPRESSION -> VALUE OPERATOR VALUE : {'$2', '$1', '$3'}.
EXPRESSION -> VALUE : '$1'.
EXPRESSION_TAIL -> OPERATOR VALUE : {'$1', '$2'}.
EXPRESSION_TAIL -> OPERATOR VALUE EXPRESSION_TAIL : {'$1', '$2', '$3'}.
VALUE -> SYMBOL : '$1'.
VALUE -> expr : '$1'.
VALUE -> PARENS : '$1'.
VALUE -> func FUNC_EXPRESSION_LIST : {'$1', '$2'}.

OPERATOR -> op : '$1'.
OPERATOR -> '=' : {_, Line} = '$1', {op, Line, "="}.
OPERATOR -> '!' : {_, Line} = '$1', {op, Line, "!"}.
OPERATOR -> '-' : {_, Line} = '$1', {op, Line, "-"}.
OPERATOR -> '+' : {_, Line} = '$1', {op, Line, "+"}.
