defmodule DistributedMumpsProgram.Runtime do
  alias DistributedMumpsProgram.{GVarServer, LockServer}
  @moduledoc """
  Documentation for DistributedMumpsProgram.
  """
  def set(arguments, state) do
    set_args(arguments, state)
  end

  defp set_args([], state), do: state
  defp set_args([{{:gvar, {:symbol, _, symbol}}, expr} | t], state) do
    value = expression(expr, state)
    GVarServer.put([symbol], value)
    set_args(t, state)
  end
  defp set_args([{{:local, {:symbol, _, symbol}}, expr} | t], state) do
    value = expression(expr, state)
    set_args(t, set_symbol_value(symbol, [], value, state))
  end
  defp set_args([{{:gvar, {:symbol, _, symbol}, subscripts}, expr} | t], state) do
    value = expression(expr, state)
    subscripts = Enum.map(subscripts, fn(expr) -> expression(expr, state) end)
    GVarServer.put([symbol | subscripts], value)
    set_args(t, state)
  end
  defp set_args([{{:local, {:symbol, _, symbol}, subscripts}, expr} | t], state) do
    value = expression(expr, state)
    set_args(t, set_symbol_value(symbol, subscripts, value, state))
  end

  def kill(arguments, state) do
    kill_args(arguments, state)
  end

  defp kill_args([], state), do: state
  defp kill_args([{:symbol, _, symbol} | t], state = %{:symbols => symbols}) do
    [_ | map_symbol] = Map.get(symbols, symbol, [nil])
    symbols = if map_symbol == [] do
      Map.delete(symbols, symbol)
    else
      Map.put(symbols, symbol, map_symbol)
    end
    kill_args(t, %{state | symbols: symbols})
  end
  defp kill_args([{:gvar, s} | t], state) do
    kill_args([{:gvar, s, []} | t], state)
  end
  defp kill_args([{:gvar, {:symbol, _, symbol}, subscripts} | t], state) do
    subscripts = Enum.map(subscripts, fn(expr) -> expression(expr, state) end)
    GVarServer.kill([symbol | subscripts])
    kill_args(t, state)
  end

  def new(arguments, state) do
    new_args(arguments, state)
  end

  defp new_args([], state), do: state
  defp new_args([{:symbol, _, symbol} | t], state = %{:symbols => symbols}) do
    map_symbol = Map.get(symbols, symbol, [])
    symbols = if map_symbol == [] do
      symbols
    else
      Map.put(symbols, symbol, ['' | map_symbol])
    end
    new_args(t, %{state | symbols: symbols})
  end


  def write(arguments, state) do
    write_args(arguments, state)
  end

  defp write_args([], state), do: state
  defp write_args(['\n' | t], state) do
    IO.puts("")
    write_args(t, state)
  end
  defp write_args([expr | t], state) do
    expression(expr, state)
    |> to_string
    |> IO.write
    write_args(t, state)
  end

  def if_statement({condition, action}, state) do
     if_statement({condition, action, {:else_token, []}}, state)
  end

  def if_statement({condition, action, {:else_token, else_case}}, state) do
    value = expression(condition, state)
    if value != '' && value != '0' do
      DistributedMumpsProgram.handle_commands(action, state)
    else
      DistributedMumpsProgram.handle_commands(else_case, state)
    end
  end

  def for_statement({{:local, {:symbol, _, symbol}}, start, increment, finish, action}, state) do
    current = expression(start, state) |> get_num
    finish = expression(finish, state) |> get_num
    inc = expression(increment, state) |> get_num
    for_statement({symbol, current, start, inc, finish, action},
      set_symbol_value(symbol, [], expression(start, state), state))
  end

  def for_statement({symbol, current, start, increment, finish, action}, state)
      when current != finish do
    state = DistributedMumpsProgram.handle_commands(action, state)
    current = current + increment
    for_statement({symbol, current, start, increment, finish, action},
      set_symbol_value(symbol, [], to_string(current), state))
  end
  def for_statement({_, _, _, _, _, action}, state) do
    DistributedMumpsProgram.handle_commands(action, state)
  end

  def lock(args, state) do
    lock_args(args, state)
  end

  def lock_args([], state), do: state
  def lock_args([{{:inc, {_, {:symbol, _, lock}}}} | t], state = %{:locks => locks}) do
    lock_value = if Map.has_key?(locks, lock) do
      Map.get(locks, lock) + 1
    else
      case LockServer.get(lock) do
        :ok -> 1
        :wait -> receive do
          :ok -> 1
        end
      end
    end
    locks = Map.put(locks, lock, lock_value)
    lock_args(t, %{state | :locks => locks})
  end

  def lock_args([{{:dec, {_, {:symbol, _, lock}}}} | t], state = %{:locks => locks}) do
    if Map.has_key?(locks, lock) do
      lock_value = Map.get(locks, lock) - 1
      if lock_value == 0 do
        LockServer.drop(lock)
        locks = Map.delete(locks, lock)
        lock_args(t, %{state | :locks => locks})
      else
        locks = Map.put(locks, lock, lock_value)
        lock_args(t, %{state | :locks => locks})
      end
    else
      lock_args(t, state)
    end
  end

  def hang(expr, state) do
     timeout = expr
     |> expression(state)
     |> get_num
     |> round

     timeout = timeout * 1000
     :timer.sleep(timeout)
     state
  end

  defp set_symbol_value(symbol, subscripts, value, state = %{:symbols => symbols}) do
    map_symbols =  Map.get(symbols, symbol, [tree: :gb_trees.empty, value: [""]])
    subscripts = resolve_symbols(subscripts, state)
    value = set_symbol_subscript_value(subscripts, value, map_symbols, state)
    symbols = Map.put(symbols, symbol, value)
    %{state | symbols: symbols}
  end

  defp set_symbol_subscript_value(subscripts, value, symbol_object, state)
  defp set_symbol_subscript_value([h | t], value, symbol_object, state) do
    # Resolve the value of h
    #  Note that this is where side effects can come in
    h = expression(h, state)
    tree = symbol_object[:tree]
    defined = :gb_trees.is_defined(h, tree)
    v = if defined do
      :gb_trees.get(h, tree)
    else
      [tree: :gb_trees.empty, value: [""]]
    end
    v = set_symbol_subscript_value(t, value, v, state)
    tree = if defined do
      :gb_trees.update(h, v, tree)
    else
      :gb_trees.insert(h, v, tree)
    end
    [tree: tree, value: value]
  end
  defp set_symbol_subscript_value([], value, symbol_object, state) do
    [tree: symbol_object[:tree], value: [value] ++ symbol_object[:value]]
  end

  defp get_symbol_subscript_value(subscripts, symbol_object, state)
  defp get_symbol_subscript_value([h | t], symbol_object, state) do
    # Resolve the value of h
    #  Note that this is where side effects can come in
    h = expression(h, state)
    tree = symbol_object[:tree]
    defined = :gb_trees.is_defined(h, tree)
    if defined do
      get_symbol_subscript_value(t, :gb_trees.get(h, tree), state)
    else
      ""
    end

  end
  defp get_symbol_subscript_value([], symbol_object, _state) do
    symbol_object[:value]
  end

  def expression(statement, state)

  # Most simple cases; leaf node
  def expression({:local, {:symbol, _, value}}, _state = %{:symbols => symbols}) do
    [tree: _, value: [value | _]] =  Map.get(symbols, value, [tree: :gb_trees.empty, value: [""]])
    value
  end
  def expression({:local, {:symbol, _, value}, subscripts}, state = %{:symbols => symbols}) do
    symbol_object =  Map.get(symbols, value, [tree: :gb_trees.empty, value: [""]])
    get_symbol_subscript_value(subscripts, symbol_object, state)
  end
  def expression({:expr, _, value}, _state) do
    value
  end
  def expression({:gvar, {:symbol, line, value}}, state) do
    expression({:gvar, {:symbol, line, value}, []}, state)
  end
  def expression({:gvar, {:symbol, _, value}, subscripts}, state) do
    subscripts = resolve_symbols(subscripts, state)
    ret = GVarServer.get([value | subscripts])
  end

  defp resolve_symbols(symbols, state)
  defp resolve_symbols([], state), do: []
  defp resolve_symbols(symbols = [h | t], state) do
    [expression(h, state)] ++ resolve_symbols(t, state)
  end
  def expression({{:func, _, name}, arguments}, state) do
    func(name, arguments, state)
  end

  # More complicated
  def expression({{:op, _, op}, expr1, expr2,}, state) do
    value1 = expression(expr1, state)
    value2 = expression(expr2, state)
    to_charlist do_op(op, value1, value2)
  end
  def expression({{:op, _, op}, expr1, expr2, tail}, state) do
    value1 = expression(expr1, state)
    value2 = expression(expr2, state)
    result = do_op(op, value1, value2)
    expression(result, tail, state)
  end
  defp expression(value1, {{:op, _, op}, expr1, tail}, state) do
    value2 = expression(expr1, state)
    result = do_op(op, value1, value2)
    expression(result, tail, state)
  end
  defp expression(value1, {{:op, _, op}, expr1}, state) do
    value2 = expression(expr1, state)
    to_charlist  do_op(op, value1, value2)
  end

  defp do_op('=', value1, value2) do
     if value1 == value2, do: 1, else: 0
  end
  defp do_op('!', value1, value2) do
    if get_bool(value1) || get_bool(value2) do
      1
    else
      0
    end
  end
  defp do_op('&', value1, value2) do
    if get_bool(value1) && get_bool(value2) do
      1
    else
      0
    end
  end

  defp do_op(op, value1, value2) do
    v1 = get_num(value1)
    v2 = get_num(value2)
    result = case op do
      '+' -> v1 + v2
      '-' -> v1 - v2
      '/' -> v1 / v2
      '*' -> v1 * v2
      '<' -> if v1 < v2, do: 1, else: 0
      '>' -> if v1 > v2, do: 1, else: 0
      '<=' -> if v1 <= v2, do: 1, else: 0
      '>=' -> if v1 >= v2, do: 1, else: 0
      '\'<' -> if v1 >= v2, do: 1, else: 0
      '\'>' -> if v1 <= v2, do: 1, else: 0
      true -> nil
    end
    if result == Kernel.trunc(result) do
      # Integer; cast as such
      round(result)
    else
      result
    end
  end

  defp get_bool(value) do
    !(value == '' || get_num(value) == 0)
  end

  defp get_num(value) do
    if value == '' || value == "" do
      0
    else
      {v, _} = Float.parse(to_string(value))
      v
    end
  end

  def func('$RANDOM', [expr], state) do
    expression(expr, state)
    |> get_num
    |> round
    |> :rand.uniform
  end
end
