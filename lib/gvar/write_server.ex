defmodule DistributedMumpsProgram.GVar.WriteServer do
  use GenServer

  def init(name) do
    {:ok, fd} = :file.open(name, [:write, :read, :binary, :raw])
    {:ok, %{fd: fd}}
  end

  def handle_call({:get, block, length}, _from, state = %{:fd => fd}) do
    {:ok, value} = :file.pread(fd, block, length)
    {:reply, value, state}
  end
  def handle_call(request, from, state) do
  # Call the default implementation from GenServer
    super(request, from, state)
  end

  def handle_cast({:put, block, value}, state = %{:fd => fd}) do
    :ok = :file.pwrite(fd, block, value)
    {:noreply, state}
  end
  def handle_cast(request, state) do
    super(request, state)
  end

  def start_link(name \\ '') do
    {:ok, id} = GenServer.start_link(__MODULE__, name)
    id
  end

  def make_instance(name \\ '') do
    pid = start_link(name)
    %{
      pid: pid,
      call: fn(args) -> GenServer.call(pid, args) end,
      cast: fn(args) -> GenServer.cast(pid, args) end,
      get: fn(block, length) -> GenServer.call(pid, {:get, block, length}) end,
      put: fn(block, value) -> GenServer.cast(pid, {:put, block, value}) end
    }
  end
end
