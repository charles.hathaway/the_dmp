defmodule DistributedMumpsProgram.GVar.Index do
  alias DistributedMumpsProgram.GVar.{Block, Index}
  defstruct version: 1, free_blocks: [], keys: [tree: :gb_trees.empty, block: %Block{}], end_block: 0, filepointer: nil

  def get_value(subscripts, index) do
    block = _get_value(subscripts, index.keys)
    if block == nil do
      nil
    else
      %Block{size: size, location: block} = block
      value = index.filepointer.get.(block, size)
      {_subscripts, value} = decode(value)
      {value, size}
    end
  end
  defp _get_value([], index), do: index[:block]
  defp _get_value([h | t], index) do
    if index == nil || :gb_trees.is_defined(h, index[:tree]) do
      v = :gb_trees.get(h, index[:tree])
      _get_value(t, v)
    else
      nil
    end
  end
  def get_block(subscripts, index) do
    block = _get_value(subscripts, index.keys)
    if block == nil do
      nil
    else
      block
    end
  end
  defp _get_block([], index), do: index[:block]
  defp _get_block([h | t], index) do
    if index == nil || :gb_trees.is_defined(h, index[:tree]) do
      v = :gb_trees.get(h, index[:tree])
      _get_block(t, v)
    else
      nil
    end
  end

  def set_value(subscripts, value, index) do
    [tree: _index2, block: block] = _find_value(subscripts, index.keys)
    {free_blocks, end_block} = if block != nil && block.size + block.location == index.end_block do
      {index.free_blocks, block.location}
    else
      free_blocks = if block == nil, do: index.free_blocks, else: index.free_blocks ++ [block]
      {free_blocks, index.end_block}
    end
    block = if block == nil, do: [], else: [block]
    {size, value} = encode(subscripts, value)
    {free_blocks, block_o = %{:location => block, :size => _size}, end_block} = find_block(size, free_blocks, end_block)
    if index.filepointer != nil do
      index.filepointer.put.(block, value)
    end
    [tree: tree, block: _block] = _set_value(subscripts, index.keys, block_o)
    %{index | keys: [tree: tree, block: nil], free_blocks: free_blocks, end_block: end_block}
  end
  defp _find_value([], index) do
    if index == nil do
      # New object, append it to the first block that has room
      [tree: :gb_trees.empty, block: nil]
    else
      [tree: index[:tree], block: index[:block]]
    end
  end
  defp _find_value([h | t], index) do
    defined = :gb_trees.is_defined(h, index[:tree])
     v = if defined do
       :gb_trees.get(h, index[:tree])
     else
       [tree: :gb_trees.empty, block: nil]
     end
     r = _find_value(t, v)
     t = if defined do
       :gb_trees.update(h, r, index[:tree])
     else
       :gb_trees.insert(h, r, index[:tree])
     end
     [tree: t, block: v[:block]]
  end

  defp _set_value([], index, value) do
    if index == nil do
      # New object, append it to the first block that has room
      [tree: :gb_trees.empty, block: value]
    else
      [tree: index[:tree], block: value]
    end
  end
  defp _set_value([h | t], index, value) do
    defined = :gb_trees.is_defined(h, index[:tree])
     v = if defined do
       :gb_trees.get(h, index[:tree])
     else
       [tree: :gb_trees.empty, block: nil]
     end
     r = _set_value(t, v, value)
     t = if defined do
       :gb_trees.update(h, r, index[:tree])
     else
       :gb_trees.insert(h, r, index[:tree])
     end
     [tree: t, block: v[:block]]
  end

  def kill_value(subscripts, index = %Index{free_blocks: free_blocks, end_block: end_block}) do
    v = get_block(subscripts, index)
    if v == nil do
      index
    else
      %Block{size: size, location: block} = v
      if block + size == end_block do
        %{index | end_block: block}
      else
        %{index | :free_blocks => free_blocks ++ [block]}
      end
    end
  end

  def read_index(fp) do
     value = _read_index(fp)
    :erlang.binary_to_term(value)
  end
  defp _read_index(fp) do
    case :file.read(fp, 4096) do
      {:ok, value} -> value <> _read_index(fp)
      :eof -> << >>
    end
  end
  def write_index(fp, index) do
    index = %{index | :filepointer => nil}
    :file.write(fp, :erlang.term_to_binary(index))
  end

  # Returns {free_blocks, block, end_block}
  #          [%Block{}], %Block{}, Int
  defp find_block(space, free_blocks, end_block, skipped_blocks \\ [])
  defp find_block(space, [], end_block, skipped_blocks) do
    {skipped_blocks, %Block{location: end_block, size: space}, end_block + space}
  end
  defp find_block(space, [h = %Block{:location => loc, :size => size} | t], end_block, skipped_blocks) do
    if space < size do
      [block | other_blocks] = if size == space do
        [%Block{location: loc, size: space}]
      else
        [%Block{location: loc, size: space}, %Block{location: loc+space, size: space-size}]
      end
      {skipped_blocks ++ other_blocks ++ t, block, end_block}
    else
      find_block(space, t, end_block, skipped_blocks ++ [h])
    end
  end

  defp encode(subscripts, value) do
    v = :erlang.term_to_binary({subscripts, to_string(value)})
    s = byte_size(v)
    v = << s :: size(64) >> <> v
    s = byte_size(v)
    {s, v}
  end
  defp decode(value) do
    << _s :: size(64), rest :: binary >> = value
    :erlang.binary_to_term(rest)
  end
end
