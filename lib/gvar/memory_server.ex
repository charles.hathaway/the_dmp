defmodule DistributedMumpsProgram.GVar.MemoryServer do
  use GenServer

  def init(value) do
    {:ok, fd} = :file.open(value, [:write, :read, :binary, :ram])
    {:ok, %{fd: fd}}
  end

  def handle_call({:get, block, length}, _from, state = %{:fd => fd}) do
    {:ok, value} = :file.pread(fd, block, length)
    {:reply, value, state}
  end
  def handle_call(request, from, state) do
  # Call the default implementation from GenServer
    super(request, from, state)
  end

  def handle_cast({:put, block, value}, state = %{:fd => fd}) do
    :ok = :file.pwrite(fd, block, value)
    {:noreply, state}
  end
  def handle_cast(request, state) do
    super(request, state)
  end

  def start_link(initial_value \\ '') do
    {:ok, id} = GenServer.start_link(__MODULE__, initial_value)
    id
  end

  def make_instance(initial_value \\ '') do
    pid = start_link(initial_value)
    %{
      pid: pid,
      call: fn(args) -> GenServer.call(pid, args) end,
      cast: fn(args) -> GenServer.cast(pid, args) end,
      get: fn(block, length) -> GenServer.call(pid, {:get, block, length}) end,
      put: fn(block, value) -> GenServer.cast(pid, {:put, block, value}) end
    }
  end
end
