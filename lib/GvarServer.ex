defmodule DistributedMumpsProgram.GVarServer do
  use GenServer

  alias DistributedMumpsProgram.GVar.{Index, WriteServer}

  def init({get_file_server_fn, cache}), do: {:ok, {get_file_server_fn, get_file_server_fn.("abc"), cache}}

  def handle_call({:get, gvar}, _from, {get_file_server_fn, index, cache}) do
    gvar = Enum.map(gvar, fn(g) -> to_charlist(g) end)
    {value, _} = if Map.has_key?(cache, gvar)  do
      {Map.get(cache, gvar), -1}
    else
      v = Index.get_value(gvar, index)
      if v == nil, do: {"", -1}, else: v
    end
    Map.put(cache, gvar, value)
    {:reply, value, {get_file_server_fn, index, cache}}
  end
  def handle_call({:kill, gvar}, _from, state = {get_file_server_fn, index, cache}) do
    gvar = Enum.map(gvar, fn(g) -> to_charlist(g) end)
    index = Index.kill_value(gvar, index)
    {:reply, index, {get_file_server_fn, index, cache}}
  end
  def handle_call(:indexs, _from, state = {_get_file_server_fn, index, _cache}) do
    {:reply, index, state}
  end
  def handle_call(:cache, _from, state = {_get_file_server_fn, _index, cache}) do
    {:reply, cache, state}
  end
  def handle_call(request, from, state) do
    super(request, from, state)
  end

  def handle_cast({:put, gvar, value}, {get_file_server_fn, index, cache}) do
    gvar = Enum.map(gvar, fn(g) -> to_charlist(g) end)
    index = Index.set_value(gvar, value, index)
    cache = Map.put(cache, gvar, value)
    {:noreply, {get_file_server_fn, index, cache}}
  end

  def handle_cast(:write_indxs, {get_file_server_fn, index, cache}) do
    _write_indexs([{"Abc", index}])
    {:noreply, {get_file_server_fn, index, cache}}
  end

  def handle_cast(request, state) do
    super(request, state)
  end

  def start_link(cache \\ %{}) do
    GenServer.start_link(__MODULE__, { &single_fileserver/1, cache}, name: __MODULE__)
  end

  def get(gvar), do: GenServer.call(__MODULE__, {:get, gvar})
  def kill(gvar), do: GenServer.call(__MODULE__, {:kill, gvar})
  def put(gvar, value) do
    GenServer.cast(__MODULE__, {:put, gvar, value})
  end
  def get_indexs(), do: GenServer.call(__MODULE__, :indexs)
  def write_indexs() do
    GenServer.cast(__MODULE__, :write_indxs)
  end

  @doc """
  """
  def single_fileserver(_gvar) do
    index_filename = "DistributedMumpsProgram.idx"
    index = if File.exists?(index_filename) do
      {:ok, fp} = :file.open(index_filename, [:read, :binary, :raw])
      Index.read_index(fp)
    else
      %Index{}
    end
    %{index | :filepointer => WriteServer.make_instance("DistributedMumpsProgram.dat")}
  end

  def global_to_fileserver(gvar) do
    gvar = to_string(gvar)
    index_filename = gvar <> ".idx"
    index = if File.exists?(index_filename) do
      {:ok, fp} = :file.open(index_filename, [:read, :binary, :raw])
      Index.read_index(fp)
    else
      %Index{}
    end
    %{index | :filepointer => WriteServer.make_instance(gvar <> ".dat")}
  end

  defp _write_indexs([]), do: :ok
  defp _write_indexs([{gvar, h} | t]) do
    gvar = to_string(gvar)
    index_filename = "DistributedMumpsProgram.idx"
    {:ok, fp} = :file.open(index_filename, [:write, :binary, :raw])
    Index.write_index(fp, h)
    :ok = :file.close(fp)
    _write_indexs(t)
  end
end
