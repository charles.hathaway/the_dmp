defmodule Profile do
  import ExProf.Macro

  def go do
    profile do
      #DistributedMumpsProgram.run_line('FOR j=1:1:1000000 S ^j=j')
      spawn_stuff(200, 0)
    end
  end

  def spawn_stuff(0, m, func \\ &function_to_spawn/0), do: wait(m)
  def spawn_stuff(n, m, func) do
    pid = spawn(func)
    Process.monitor(pid)
    spawn_stuff(n-1, m + 1, func)
  end

  defp wait(0), do: :ok
  defp wait(n) do
    receive do
      _ -> wait(n-1)
    end
  end

  defp function_to_spawn() do
    DistributedMumpsProgram.run_line('FOR j=1:1:10 S T=$RANDOM(100),Z=$RANDOM(10) LOCK +^R IF T<50 S ^R(Z)=T ELSE S temp=^R(Z) LOCK -^R')
  end
end
