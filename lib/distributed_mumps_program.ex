defmodule DistributedMumpsProgram do
  alias DistributedMumpsProgram.{Runtime}
  use Application

  @initial_state %{symbols: %{}, locks: %{}}
  @moduledoc """
  Documentation for DistributedMumpsProgram.
  """
  def run(state \\ @initial_state) do
    #IO.puts("State: #{inspect(state)}")
    input = IO.gets("DMP> ") |> to_charlist
    if input == '\n' do
      run(state)
    else
      #IO.puts input
      {:ok, tokens, _lines} = :lexer.string(input)
      res = :parser.parse(tokens)
      case res do
        {:ok, output} ->
          state = handle_commands(output, state)
          if state == :quit do
            :ok
          else
            run(state)
          end
        _ ->
          IO.puts("Bad syntax on line; ignored")
          IO.inspect(res)
          run(state)
      end
    end
  end

  def run_line(line, state \\ @initial_state) do
    {:ok, tokens, _lines} = :lexer.string(line)
    {:ok, output} = :parser.parse(tokens)
    handle_commands(output, state)
  end

  def handle_commands(commands, state)
  def handle_commands([h | t], state) do
    state = handle_command(h, state)
    handle_commands(t, state)
  end
  def handle_commands([], state), do: state

  def handle_command(command, state)
  def handle_command({:set, arguments}, state), do: Runtime.set(arguments, state)
  def handle_command({:kill, arguments}, state), do: Runtime.kill(arguments, state)
  def handle_command({:new, arguments}, state), do: Runtime.new(arguments, state)
  def handle_command({:write, arguments}, state), do: Runtime.write(arguments, state)
  def handle_command({:if_token, arguments}, state), do: Runtime.if_statement(arguments, state)
  def handle_command({:for_token, arguments}, state), do: Runtime.for_statement(arguments, state)
  def handle_command({:lock, arguments}, state), do: Runtime.lock(arguments, state)
  def handle_command({:hang, arguments}, state), do: Runtime.hang(arguments, state)
  def handle_command({{:func, _, name}, arguments}, state), do: Runtime.func(name, arguments, state)
  def handle_command(:quit, state), do: :quit
  def handle_command(command, state) do
    IO.puts("Unknown command: #{IO.inspect(command)}")
    state
  end

  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start your own worker by calling: Flycker.Worker.start_link(arg1, arg2, arg3)
      # worker(Flycker.Worker, [arg1, arg2, arg3]),
      worker(DistributedMumpsProgram.GVarServer, []),
      worker(DistributedMumpsProgram.LockServer, [])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: DistributedMumpsProgram.Supervisor]
    Supervisor.start_link(children, opts)
  end

end
