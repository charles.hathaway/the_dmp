defmodule DistributedMumpsProgram.LockServer do
  use GenServer

  @initial_state  %{has_lock: %{}, wants_lock: %{}}

  def init(state), do: {:ok, state}

  def handle_call({:get, lock}, from, state = %{:wants_lock => wl, :has_lock => hl}) do
    # If someone has the key, besides the caller, tell them to wait
    holder = Map.get(hl, lock)
    if holder != nil && holder != from do
      wl_lock = Map.get(wl, lock, [])
      if Enum.member?(wl_lock, from) do
        # The shouldn't be here, yell at them
        raise "Requesting lock without waiting your turn, bad bad"
      else
        {:reply, :wait, %{state | :wants_lock => Map.put(wl, lock, wl_lock ++ [from])}}
      end
    else
      # Give that person the lock!
      {:reply, :ok, %{state | :has_lock => Map.put(hl, lock, from)}}
    end
  end
  def handle_call({:dump}, _from, state) do
    {:reply, {:ok, state}, state}
  end
  def handle_call({:clear}, _from, _state) do
    {:reply, {:ok, @initial_state}, @initial_state}
  end
  def handle_call({:clear, lock}, _from, state = %{:has_lock => hl, :wants_lock => wl}) do
    wl = Map.delete(wl, lock)
    hl = Map.delete(hl, lock)
    state = %{state | :wants_lock => wl, :has_lock => hl}
    {:reply, {:ok, state}, state}
  end
  def handle_cast({:drop, lock}, state) do
    state = %{state | :has_lock => Map.delete(state.has_lock, lock)}
    wants_lock = state.wants_lock
    wants_lock_list = Map.get(wants_lock, lock, [])
    state = if wants_lock_list != [] do
      {next, left} = List.pop_at(wants_lock_list, 0)
      hl = Map.put(state.has_lock, lock, next)
      {pid, _} = next
      send(pid, :ok)
      %{state | :wants_lock => Map.put(wants_lock, lock, left), :has_lock => hl}
    else
      state
    end
    {:noreply, state}
  end

  def start_link(state \\ @initial_state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def get(lock), do: GenServer.call(__MODULE__, {:get, lock})
  def drop(lock) do
    GenServer.cast(__MODULE__, {:drop, lock})
  end

  def get_lock_table, do: GenServer.call(__MODULE__, {:dump})
  def clear_lock_table, do: GenServer.call(__MODULE__, {:clear})
  def clear_lock(lock), do: GenServer.call(__MODULE__, {:clear, lock})
end
