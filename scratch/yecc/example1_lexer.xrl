Definitions.

D = [0-9]

Rules.

\( : {token,{'(', TokenLine}}.
\) : {token,{')', TokenLine}}.
[A-Za-z][A-Za-z0-9]+ : {token, {atom, TokenLine, list_to_atom(TokenChars)}}.
\n : {token, {'$end', TokenLine}}.
[\s\t] : skip_token.

{D}+ :
  {token,{integer,TokenLine,list_to_integer(TokenChars)}}.

{D}+\.{D}+((E|e)(\+|\-)?{D}+)? :
  {token,{float,TokenLine,list_to_float(TokenChars)}}.

Erlang code.
